#!/bin/bash

# il faut préserver node_modules

rmall(){
    rm -fr src target webpack *.xml *.js *.json pom.xml package.json
    rm -fr .yo-rc.json
    rm -fr .jhipster
}

SRC=../common/layout
cp $SRC/.yo-rc.json .yo-rc.json
jhipster


cpclient(){
    unalias cp
    unalias rm
    SRC=../common/layout
    rm -f src/main/webapp/content/images/*jhipster*.*
    cp -f $SRC/loading.css src//main/webapp/content/css/loading.css
    cp -f $SRC/polytech.svg src/main/webapp/content/images/polytech.svg
    cp -f $SRC/home.component.html src/main/webapp/app/home/home.component.html
    cp -f $SRC/home.scss src/main/webapp/app/home/home.scss
    cp -f $SRC/global.json src/main/webapp/i18n/fr/global.json
    cp -f $SRC/home.json src/main/webapp/i18n/fr/home.json
    cp -f $SRC/navbar.scss src/main/webapp/app/layouts/navbar/navbar.scss
}

cpserver(){
    unalias cp
    unalias rm
    SRC=../common/layout
    cp -f $SRC/banner.txt src/main/resources/banner.txt
    cp -f $SRC/authority.csv src/main/resources/config/liquibase/data/authority.csv
}

