# Yitus :: Generate the projects

## nginx

https://www.jhipster.tech/separating-front-end-and-api/ 

## update jhipster
    npm install -g generator-jhipster@6.9.0
    jhipster --version

## creation 1 backend
    cd gitlab/yitus/backend
    cp ../common/layout/.yo-rc.json .
    jhipster --skip-client
    jhipster import-jdl ../common/jdl/schema.jh
    ./mvnw

## creation frontend
    FRONTEND=frontend
    mkdir -p ~/gitlab/yitus/$FRONTEND
    cd ~/gitlab/yitus/$FRONTEND
    cp ../common/layout/.yo-rc.json .
    jhipster --skip-server --auth jwt --db postgresql
    jhipster import-jdl ../common/jdl/schema.jh
    yarn start
    # build PROD version
    yarn build

## creation frontend for admin
    FRONTEND=admin
    mkdir -p ~/gitlab/yitus/$FRONTEND
    cd ~/gitlab/yitus/$FRONTEND
    cp ../common/layout/.yo-rc.json .
    jhipster --skip-server --auth jwt --db postgresql
    jhipster import-jdl ../common/jdl/schema.jh
    yarn start
    # build PROD version
    yarn build


# Get the API description

    curl http://localhost:8080/v2/api-docs | jq . > swagger.json

# Running the service

## Depuis la machine de dev

    cd gitlab/yitus/app
    # npm install # si de nouveaux packages sont ajoutés au frontend
    ./mvnw -Pprod -DskipTests  package
    scp target/yitus-0.0.1-SNAPSHOT.jar  <host>/yitus/app/target


## Depuis la machine de prod
    cd yitus/app
    docker-compose -f src/main/docker/postgresql.yml up -d
    docker-compose -f src/main/docker/postgresql.yml ps
    nohup java -Dspring.profiles.active=prod -jar target/yitus-0.0.1-SNAPSHOT.jar &
    # Remarque: il est preferable de déployer le container Docker construit avec ./mvnw -Pprod jib:dockerBuild
    

> Remarque: en cas de changement de schéma de la BD, il faut destruire le container PostgreSQL.
